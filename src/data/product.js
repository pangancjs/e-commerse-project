export const product = [

    {
      "image": "../images/product.png",
      "name": "Laptop1",
      "categorie": "Asus",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 2",
      "categorie": "Asus",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 3",
      "categorie": "Acer",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 4",
      "categorie": "Acer",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 5",
      "categorie": "Dell",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 6",
      "categorie": "Dell",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 7",
      "categorie": "Razer",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/product.png",
      "name": "Laptop 8",
      "categorie": "Razer",
      "active" : true,
      "price": 40000
    },
    {
      "image": "../images/Mouse.png",
      "name": "Mouse 1",
      "categorie": "Mouse",
      "active" : true,
      "price": 12000
    },
    {
      "image": "../images/Mouse.png",
      "name": "Mouse 2",
      "categorie": "Mouse",
      "active" : true,
      "price": 12000
    },
    {
      "image": "../images/Keyboard.jpg",
      "name": "Keyboard 1",
      "categorie": "Keyboard",
      "active" : true,
      "price": 13000
    },
    {
      "image": "../images/Keyboard.jpg",
      "name": "Keyboard 2",
      "categorie": "Keyboard",
      "active" : true,
      "price": 13000
    },
    {
      "image": "../images/Monitor.jpg",
      "name": "Monitor 1",
      "categorie": "Monitor",
      "active" : true,
      "price": 20000
    },
    {
      "image": "../images/Monitor.jpg",
      "name": "Monitor 2",
      "categorie": "Monitor",
      "active" : true,
      "price": 20000
    },
    {
      "image": "../images/Motherboard.png",
      "name": "Motherboard 1",
      "categorie": "Motherboard",
      "active" : true,
      "price": 15000
    },
    {
      "image": "../images/Motherboard.png",
      "name": "Motherboard 2",
      "categorie": "Motherboard",
      "active" : true,
      "price": 15000
    },
    {
      "image": "../images/Ram.jpg",
      "name": "Ram 1",
      "categorie": "Ram",
      "active" : true,
      "price": 9000
    },
    {
      "image": "../images/Ram.jpg",
      "name": "Ram 2",
      "categorie": "Ram",
      "active" : true,
      "price": 9000
    },
    
]