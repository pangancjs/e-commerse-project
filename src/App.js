import Home from "./pages/Home"
import Shop from "./pages/Shop"
import Navbar from "./components/Navbar"
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

function App() {
  return (
    <>
    <Navbar/>
     <Router>
       <Switch >
        <Route exact path="/">
          <Home/>
        </Route>
        <Route exact path="/shop">
          <Shop/>
        </Route>
       </Switch>
     </Router>
     </>
  );
}

export default App;