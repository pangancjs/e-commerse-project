import { Container, Row, Col } from "react-bootstrap"
import './../styles/style.css'
export default function Banner() {
  return (
    <Container fluid>
      <Row>
        <Col className="col-md-6 col-12 p-1">
          <div class="banner">
            <img src="https://www.hanwhasecurity.com/media/catalog/category/monitors_banner_12.png" alt="" />
            <div class="centered">
            <button>Monitor</button>
            </div>
          </div>
        </Col>
        <Col className="col-md-6 col-12 p-1  ">
          <div class="banner">
            <img src="http://www.hamaan.in/wearables/img/headphone-h-20/first-banner.jpg" alt="" />
            <div class="centered">
            <button>Headphones</button>
            </div>
          </div>
        </Col>
        <Col className="col-md-6 col-12 p-1  ">
          
          <div class="banner">
            <img src="https://cdn.shopify.com/s/files/1/1780/7915/files/Motherboard.jpg?280717590019317677" alt="" />
            <div class="centered">
              <button>Motherboards</button>
            </div>
          </div>
        </Col>
        <Col className="col-md-6 col-12 p-1 ">
         
          <div class="banner">
            <img src="https://i0.wp.com/www.funkykit.com/wp-content/uploads/2020/06/ACER-predator-gaming-laptop-banner.jpg?fit=2560%2C1080&ssl=1" alt="" />
            <div class="centered">
            <button>Laptop</button>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  )
}