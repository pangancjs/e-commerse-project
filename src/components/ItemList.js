import Carousel, { slidesToShowPlugin } from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { Card,Button } from 'react-bootstrap';
import laptop from "./../images/PngItem_719768.png"
import { product } from './../data/product'
import { useEffect,useState } from 'react';
export default function SimpleSlider() {
  const [data, setData] = useState([])
  const carouselData =  data?.map(item => {
           
    return (
   
      <Card className="border-0 shadow-none" style={{ height: '19rem',width: '12rem', marginTop: '1rem' }}>
        <Card.Img variant="top" src={item.image} style={{padding: '1rem', height: '50%'}}/>
        <Card.Body>
          <Card.Title>{item.categorie}</Card.Title>
          <Card.Title>{item.price}</Card.Title>
          <Card.Text>
          </Card.Text>
          <Button variant="primary">Add Cart</Button>
        </Card.Body>
      </Card> 
    )
  })
  useEffect(() => {
    setData(product)
  }, [])
  return (
<Carousel
  plugins={[
    'arrows',
    {
      resolve: slidesToShowPlugin,
      options: {
       numberOfSlides: 5
      }
    },
  ]}
  breakpoints={{
    640: {
      plugins: [
       {
         resolve: slidesToShowPlugin,
         options: {
          numberOfSlides: 1
         }
       },
     ]
    },
    900: {
      plugins: [
       {
         resolve: slidesToShowPlugin,
         options: {
          numberOfSlides: 2
         }
       },
     ]
    },
    1024: {
      plugins: [
       {
         resolve: slidesToShowPlugin,
         options: {
          numberOfSlides: 4
         }
       },
     ]
    }
  }}
>
{carouselData}
</Carousel>
  );
}
