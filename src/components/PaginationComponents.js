import { useEffect, useState } from "react"
import '../styles/style.css'
import { product } from './../data/product'
import { Card, Button, Container, Row, Col } from "react-bootstrap"

export default function Pagination({ categorie }) {

  const [data, setData] = useState([])
  const [categories, setCategories] = useState(null)
  console.log(categorie)
  let dataLength;
  let newData = [];
  //sort by categorie
  if (categorie == null) {
    //filterd by catigories
    let samples = data?.map(item => item)
    const filtered = samples.filter(items => items.active == true)
    newData = filtered
    dataLength = filtered?.length
  } else {
    let samples = data?.map(item => item)
    const filtered = samples.filter(items => items.categorie == categorie)
    newData = filtered
    dataLength = filtered?.length
  }

  const [current, setCurretnPage] = useState(1)
  const [itemsPerPage, setItemsPerPage] = useState(5)
  const [pageNumberLimit, setpageNumberLimit] = useState(5)
  const [maxpageNumberLimit, setmaxpageNumberLimit] = useState(5)
  const [minpageNumberLimit, setminpageNumberLimit] = useState(0)
  const handleClick = (event) => {
    setCurretnPage(Number(event.target.id))
  }
  const renderData = data => {
    return (
        <>
          {data?.map(item => {
            return (
              <Col className="col-lg-3 ">
              <Card className="shadow-none" style={{ height: '19rem',width: '12rem', marginTop: '1rem' }}>
                <Card.Img variant="top" src={item.image} style={{padding: '1rem', height: '50%'}}/>
                <Card.Body>
                  <Card.Title>{item.categorie}</Card.Title>
                  <Card.Title>{item.price}</Card.Title>
                  <Card.Text>
                  </Card.Text>
                  <Button variant="primary">Add Cart</Button>
                </Card.Body>
              </Card>
              </Col>
            )
          })}
        </>

      
    )
  }
  const pages = [];
  for (let i = 1; i <= Math.ceil(dataLength / itemsPerPage); i++) {
    pages.push(i)
  }

  const indexOfLastItem = current * itemsPerPage
  const indexOfFirstItem = indexOfLastItem - itemsPerPage
  const currentItem = newData?.slice(indexOfFirstItem, indexOfLastItem)

  const renderPageNumbers = pages.map(number => {
    if (number < maxpageNumberLimit + 1 && number > minpageNumberLimit) {
      return (
        <li key={number} id={number} onClick={handleClick} className={current == number ? "active" : null}>{number}</li>
      )
    } else {
      return null
    }
  })
  useEffect(() => {
    setData(product)
    setCategories(categorie)
  }, [])

  const handleNextBtn = () => {
    setCurretnPage(current + 1)

    if (current + 1 > maxpageNumberLimit) {
      setmaxpageNumberLimit(maxpageNumberLimit + pageNumberLimit)
      setminpageNumberLimit(minpageNumberLimit + pageNumberLimit)
    }
  }
  const handlePrevBtn = () => {
    setCurretnPage(current - 1)

    if ((current - 1) % pageNumberLimit == 0) {
      setmaxpageNumberLimit(maxpageNumberLimit - pageNumberLimit)
      setminpageNumberLimit(minpageNumberLimit - pageNumberLimit)
    }
  }
  let pageIncrementBtn = null;
  if (pages.length > maxpageNumberLimit) {
    pageIncrementBtn = <li onClick={handleNextBtn}>&hellip;</li>
  }
  let pageDecrementtBtn = null;
  if (pages.length > maxpageNumberLimit) {
    pageDecrementtBtn = <li onClick={handlePrevBtn}>&hellip;</li>
  }
  return (
    <>
      <Container >
        <Row>
        
          {renderData(currentItem)}
        </Row>
      </Container>
      <ul className='pageNumbers'>
        <li>
          <button onClick={handlePrevBtn} disabled={current == pages[0] ? true : false}>
            Prev
          </button >
        </li>
        {pageDecrementtBtn}
        {renderPageNumbers}
        {pageIncrementBtn}
        <li>
          <button onClick={handleNextBtn} disabled={current == pages[pages.length - 1] ? true : false}>
            Next
          </button>
        </li></ul>
    </>
  )
}
