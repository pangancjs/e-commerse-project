import { Col, Row, Container, Accordion, Card, Button } from "react-bootstrap"
import Navbar from "./../components/Navbar"
import laptop from "./../images/PngItem_719768.png"
import Pagination from "../components/PaginationComponents"
import './../styles/style.css'
import { useState } from "react"
export default function Shop() {
  const [categories, setCategories] = useState()

  function  handleClick(event,props){
    event.preventDefault()
    setCategories(props)
    
  }
  return (
    <>
      {/* <Navbar /> */}
      <Container >
        <Row className="justify-content-center">
          <div id='categories'>
            <Col style={{width: '100%'}}>
              <Accordion defaultActiveKey='0'>
                <Accordion.Item eventKey="0">
                  <Accordion.Header>Components</Accordion.Header>
                  <Accordion.Body>
                    <ul>
                    <li onClick={(event) => {handleClick(event,'Mouse')}}>Mouse</li>
                    <li onClick={(event) => {handleClick(event,'Keyboard')}}>Keyboard</li>
                    <li onClick={(event) => {handleClick(event,'Monitor')}}>Monitor</li>
                    <li onClick={(event) => {handleClick(event,'Motherboard')}}>Motherboard</li>
                    <li onClick={(event) => {handleClick(event,'Ram')}}>Ram</li>
                    </ul>
                  </Accordion.Body>
                </Accordion.Item>
              </Accordion>
              <Accordion defaultActiveKey="0">
                <Accordion.Item eventKey="0">
                  <Accordion.Header>Laptop</Accordion.Header>
                  <Accordion.Body>
                    <ul>
                    <li onClick={(event) => {handleClick(event,null)}}>All</li>
                    <li onClick={(event) => {handleClick(event,'Asus')}}>Asus</li>
                    <li onClick={(event) => {handleClick(event,'Acer')}}>Acer</li>
                    <li onClick={(event) => {handleClick(event,'Dell')}}>Dell</li>
                    <li onClick={(event) => {handleClick(event,'Razer')}}>Razer</li>
                    </ul>
                  </Accordion.Body>
                </Accordion.Item>

              </Accordion>
            </Col>
          </div>
          <Col>
            <Pagination categorie={categories}/>
          </Col>
        </Row>
      </Container>
    </>
  )
}