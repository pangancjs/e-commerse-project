import img1 from "./../images/ROG_banner_pc.jpg"
import img2 from "./../images/ROG-Strix_banner_pc.jpg"
import Navbar from "./../components/Navbar"
import ItemList from "./../components/ItemList"
import Banner from "./../components/BannerLinks"
import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBCarouselElement,
  MDBFooter 
} from 'mdb-react-ui-kit';

export default function App() {
  return (
  <>
      {/* <Navbar /> */}
      <MDBCarousel fade>
        <MDBCarouselInner>
          <MDBCarouselItem itemId={0}>
            <MDBCarouselElement src={img1} alt='...' />
          </MDBCarouselItem>
          <MDBCarouselItem itemId={1}>
            <MDBCarouselElement src={img2} alt='...' />
          </MDBCarouselItem>
        </MDBCarouselInner>
      </MDBCarousel>
      <div style={{ marginTop: '3rem' }}>
        <ItemList />
      </div>
      <div>
        <Banner/>
      </div>
      <MDBFooter backgroundColor='light' className='text-center text-lg-left'>
      <div className='text-center p-3' style={{ backgroundColor: 'rgba(0, 0, 0, 0.2)' }}>
        &copy; {new Date().getFullYear()} Copyright:{' '}
        <a className='text-dark' href='https://mdbootstrap.com/'>
          MDBootstrap.com
        </a>
      </div>
    </MDBFooter>
     
     </>

      );
}
